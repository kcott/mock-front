import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MocksModule } from './mocks/mocks.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RequestMethodService } from './generics/services/request-method.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MocksModule,
    NoopAnimationsModule,
    ModalModule.forRoot()
  ],
  providers: [
    RequestMethodService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
