import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-json-editor',
  templateUrl: './json-editor.component.html',
  styleUrls: ['./json-editor.component.scss']
})
export class JsonEditorComponent implements OnInit {

  @Input() protected data: string;
  @Input() protected readOnly: boolean;

  protected options = {
    mode: 'application/ld+json',
    lineNumbers: true,
    lineWrapping: true,
    foldGutter: true,
    gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter', 'CodeMirror-lint-markers'],
    autoCloseBrackets: true,
    matchBrackets: true,
    lint: true,
    readOnly: false
  }

  constructor() { }


  ngOnInit() {
    if (this.readOnly === true) {
      this.options.readOnly = true;
    }
  }

}
