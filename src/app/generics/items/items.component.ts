import { Component, OnInit, ContentChildren, QueryList } from '@angular/core';
import { ItemComponent } from './item/item.component';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  @ContentChildren(ItemComponent)
  private items: QueryList<ItemComponent>;

  constructor() {
  }

  ngOnInit() {
  }
}
