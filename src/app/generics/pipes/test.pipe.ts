import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'test'
})
export class TestPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let value2 = '<pre><samp>' + value.replace(/\n/gi, '</samp><samp>') + '</samp></pre>';
    return value2;
  }

}
