import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-code-viewer',
  templateUrl: './code-viewer.component.html',
  styleUrls: ['./code-viewer.component.scss']
})
export class CodeViewerComponent implements OnInit {

  @Input() protected isDisplayedLines: boolean;
  @Input() protected borderColor: string;

  protected classes: string;

  constructor() { }

  ngOnInit() {
    this.classes = '';
    if (this.isDisplayedLines) {
      this.classes += 'codeViewer-lines ';
    }
    if (this.borderColor) {
      this.classes += 'codeViewer-border ';
    }
  }

}
