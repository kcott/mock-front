import { Component, OnInit, ContentChildren, QueryList, AfterContentInit, Input, Output, EventEmitter } from '@angular/core';
import { TabComponent } from './tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, AfterContentInit {

  @ContentChildren(TabComponent) private tabs: QueryList<TabComponent>;
  @Output() public click = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    let activatedTab = this.tabs.find(tab => tab.active == true);
    if (!activatedTab) {
      this.tabs.forEach(tab => tab.active = false);
      this.selectTab(this.tabs.first);
    }
  }

  protected selectTab(selectedTab: TabComponent) {
    if (selectedTab.disabled !== true) {
      this.tabs.filter(tab => tab != selectedTab).forEach(tab => tab.active = false);
      this.tabs.filter(tab => tab == selectedTab).forEach(tab => tab.active = true);
    }
  }

  protected onClick(selectedTab: TabComponent) {
    this.click.emit(selectedTab.messageOnClick);
  }

}
