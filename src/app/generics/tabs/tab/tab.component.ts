import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() protected title: string;
  @Input() public active: boolean;
  @Input() public disabled: boolean;
  @Input() public messageOnClick: string;

  constructor() { }

  ngOnInit() {
  }
}
