import { Injectable } from '@angular/core';
import { RequestMethodMockEnum } from '../enums/RequestMethodMockEnum';

export enum CssClassKeys {
    CONTENT,
    VERB
}
@Injectable({
  providedIn: 'root'
})
export class RequestMethodService {
  public cssClassKeys = CssClassKeys;

  constructor() { }

  public getCssClasses(requestMethodMockEnum: RequestMethodMockEnum) {
    var cssClasses = {};
    if (requestMethodMockEnum) {
      cssClasses[CssClassKeys.CONTENT] = 'method-request-' + requestMethodMockEnum.toLowerCase() + '-content';
      cssClasses[CssClassKeys.VERB] = 'method-request-' + requestMethodMockEnum.toLowerCase();
    }
    return cssClasses;
  }
}
