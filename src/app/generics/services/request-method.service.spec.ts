import { TestBed } from '@angular/core/testing';

import { RequestMethodService } from './request-method.service';

describe('RequestMethodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestMethodService = TestBed.get(RequestMethodService);
    expect(service).toBeTruthy();
  });
});
