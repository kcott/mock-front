import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-json-viewer',
  templateUrl: './json-viewer.component.html',
  styleUrls: ['./json-viewer.component.scss']
})
export class JsonViewerComponent implements OnChanges {
  @Input() protected data: string;
  protected jsonData: {};
  protected readOnly = true;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) {
      try {
        this.jsonData = JSON.parse(this.data);
      } catch (error) {
        console.error('Error in JsonViewerComponent in ngOnChanges function:' + error.name);
      }
    }
  }

  protected onClick(message: string) {
    if (message === 'copy') {
      const selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = this.data;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
    }
  }
}
