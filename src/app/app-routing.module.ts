import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MocksModule } from './mocks/mocks.module';

const appRoutes: Routes = [
  {
    path: 'mocks/*',
    component: MocksModule
  },
  {
    path: '',
    redirectTo: 'mocks',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
