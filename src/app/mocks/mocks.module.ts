import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { TabsModule } from 'ngx-tabset';

import { BodyMockComponent } from './components/body-mock/body-mock.component';
import { UrlsMockComponent } from './components/urls-mock/urls-mock.component';
import { UrlMockComponent } from './components/url-mock/url-mock.component';
import { BodysMockComponent } from './components/bodys-mock/bodys-mock.component';
import { ItemsComponent } from '../generics/items/items.component';
import { ItemComponent } from '../generics/items/item/item.component';
import { MocksService } from './services/mocks.service';
import { MocksRoutingModule } from './mocks-routing.module';
import { BodyMockFormComponent } from './components/body-mock-form/body-mock-form.component';
import { UrlMockFormComponent } from './components/url-mock-form/url-mock-form.component';
import { JsonViewerComponent } from '../generics/json-viewer/json-viewer.component';
import { JsonEditorComponent } from '../generics/json-editor/json-editor.component';
import { TabComponent } from '../generics/tabs/tab/tab.component';
import { TabsComponent } from '../generics/tabs/tabs.component';
import { MockComponent } from './components/mock/mock.component';

@NgModule({
  declarations: [
    BodyMockComponent,
    UrlsMockComponent,
    UrlMockComponent,
    BodysMockComponent,
    ItemsComponent,
    ItemComponent,
    BodyMockFormComponent,
    UrlMockFormComponent,
    JsonViewerComponent,
    JsonEditorComponent,
    TabsComponent,
    TabComponent,
    MockComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MocksRoutingModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CodemirrorModule,
    NgxJsonViewerModule,
    TabsModule.forChild()
  ],
  providers: [
    HttpClient,
    MocksService
  ]
})
export class MocksModule {
  constructor(private mocksService: MocksService) {
    this.mocksService.getUrlMocks().subscribe(urlMocks => this.mocksService.urlMocks = urlMocks);
  }
}
