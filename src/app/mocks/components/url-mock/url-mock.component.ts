import { Component, OnInit, TemplateRef, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { MocksService } from '../../services/mocks.service';
import { ActivatedRoute } from '@angular/router';
import { UrlMock } from '../../dtos/UrlMock';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-url-mock',
  templateUrl: './url-mock.component.html',
  styleUrls: ['./url-mock.component.scss']
})
export class UrlMockComponent implements OnInit, DoCheck {
  protected bsModalRef: BsModalRef;

  protected urlMock: UrlMock;
  protected idUrl;

  constructor(
    private bsModalService: BsModalService,
    private mocksService: MocksService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.paramMap.get('idUrl');
  }
  ngDoCheck(): void {
    if (this.mocksService.urlMocks !== undefined && this.urlMock === undefined) {
      this.urlMock = this.mocksService.urlMocks.find(urlMock => urlMock.id === this.idUrl)
    }
  }

  protected edit(template: TemplateRef<any>) {
    this.bsModalRef = this.bsModalService.show(template);
  }

  protected cssClassUrlMock(urlMock: UrlMock) {
    if (!urlMock.active) {
      return 'stripes';
    }
  }
}
