import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlMockComponent } from './url-mock.component';

describe('UrlMockComponent', () => {
  let component: UrlMockComponent;
  let fixture: ComponentFixture<UrlMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlMockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
