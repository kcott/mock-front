import { Component, OnInit, TemplateRef, Output, EventEmitter, DoCheck } from '@angular/core';
import { MocksService } from '../../services/mocks.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UrlMock } from '../../dtos/UrlMock';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RequestMethodService } from 'src/app/generics/services/request-method.service';
import { BodyMock } from '../../dtos/BodyMock';
import { RequestMethodMockEnum } from 'src/app/generics/enums/RequestMethodMockEnum';

@Component({
  selector: 'app-bodys-mock',
  templateUrl: './bodys-mock.component.html',
  styleUrls: ['./bodys-mock.component.scss']
})
export class BodysMockComponent implements OnInit, DoCheck {
  protected bsModalRef: BsModalRef;
  protected urlMock: UrlMock;
  protected idUrl;

  constructor(
    private bsModalService: BsModalService,
    private mocksService: MocksService,
    private requestMethodService: RequestMethodService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.idUrl = this.route.snapshot.paramMap.get('idUrl');
  }

  ngDoCheck(): void {
    if (this.mocksService.urlMocks !== undefined && this.urlMock === undefined) {
      this.urlMock = this.mocksService.urlMocks.find(urlMock => urlMock.id === this.idUrl)
    }
  }

  protected edit(template: TemplateRef<any>) {
    this.bsModalRef = this.bsModalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' })
    );
  }

  protected delete(id: number) {
    this.mocksService.deleteBodyMock(id).subscribe();
    this.urlMock.bodyMocks.splice(
      this.urlMock.bodyMocks.indexOf(
        this.urlMock.bodyMocks.find(body => body.id == id)
      ), 1);
  }

  protected requestMethodContentCssClass(bodyMock: BodyMock) {
    let returnClasses: string;
    let cssClasses = this.requestMethodService.getCssClasses(RequestMethodMockEnum[bodyMock.requestMethod]);
    returnClasses = cssClasses[this.requestMethodService.cssClassKeys.CONTENT];
    if (!bodyMock.active) {
      returnClasses = returnClasses + ' stripes';
    }
    return returnClasses;
  }

  protected requestMethodVerbCssClass(bodyMock: BodyMock) {
    let cssClasses = this.requestMethodService.getCssClasses(RequestMethodMockEnum[bodyMock.requestMethod]);
    return cssClasses[this.requestMethodService.cssClassKeys.VERB];
  }
}
