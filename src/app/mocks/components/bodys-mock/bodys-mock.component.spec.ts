import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodysMockComponent } from './bodys-mock.component';

describe('BodysMockComponent', () => {
  let component: BodysMockComponent;
  let fixture: ComponentFixture<BodysMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodysMockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodysMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
