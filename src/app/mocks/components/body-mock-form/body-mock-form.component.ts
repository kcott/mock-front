import { Component, OnInit, Input, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
import { MocksService } from '../../services/mocks.service';
import { BodyMock } from '../../dtos/BodyMock';
import { RequestMethodMockEnum } from '../../../generics/enums/RequestMethodMockEnum';

@Component({
  selector: 'app-body-mock-form',
  templateUrl: './body-mock-form.component.html',
  styleUrls: ['./body-mock-form.component.scss']
})
export class BodyMockFormComponent implements OnInit {
  protected requestMethodMockEnum = RequestMethodMockEnum;
  protected id: number;
  protected name: string;
  protected description: string;
  protected requestMethod: string;
  protected active: boolean;
  protected requestBody: string;
  protected responseBody: string;

  @Input() protected bsModalRef: BsModalRef;
  @Input() protected bodyMock: BodyMock;
  @Input() protected idUrlMock: number;

  protected options = {
    mode: 'application/ld+json',
    lineNumbers: true,
    lineWrapping: true,
    foldGutter: true,
    gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter', 'CodeMirror-lint-markers'],
    autoCloseBrackets: true,
    matchBrackets: true,
    lint: true,
    readOnly: false
  }

  constructor(
    private mocksService: MocksService
  ) { }

  ngOnInit() {
    this.updateBodyMock(this.bodyMock);
  }

  updateBodyMock(bodyMock: BodyMock) {
    if (bodyMock) {
      this.id = bodyMock.id;
      this.idUrlMock = bodyMock.urlMockId;
      this.name = bodyMock.name;
      this.description = bodyMock.description;
      this.requestMethod = bodyMock.requestMethod;
      this.active = bodyMock.active;
      this.requestBody = bodyMock.requestBody;
      this.responseBody = bodyMock.responseBody;
    } else {
      this.requestMethod = this.requestMethodMockEnum.GET;
    }
  }

  onSubmit() {
    const myBodyMock: BodyMock = {
      id: this.id,
      urlMockId: this.idUrlMock,
      name: this.name,
      description: this.description,
      requestMethod: this.requestMethod,
      active: this.active,
      requestBody: this.requestBody,
      responseBody: this.responseBody
    };
    if (!this.bodyMock) {
      this.mocksService.addBodyMock(myBodyMock).subscribe(bodyMock =>
        this.success(bodyMock)
      );
    } else {
      this.mocksService.updateBodyMock(myBodyMock).subscribe(bodyMock =>
        this.success(bodyMock)
      );
    }
  }

  success(bodyMock: BodyMock) {
    let urlMockInMemory = this.mocksService.urlMocks.find(url => url.id == this.idUrlMock);
    let bodyMockInMemory = urlMockInMemory.bodyMocks.find(body => body.id === bodyMock.id);
    if (bodyMockInMemory !== undefined) {
      bodyMockInMemory.name = bodyMock.name;
      bodyMockInMemory.description = bodyMock.description;
      bodyMockInMemory.requestMethod = bodyMock.requestMethod;
      bodyMockInMemory.active = bodyMock.active;
      bodyMockInMemory.requestBody = bodyMock.requestBody;
      bodyMockInMemory.responseBody = bodyMock.responseBody;
    } else {
      urlMockInMemory.bodyMocks.push(bodyMock);
    }
    this.bsModalRef.hide();
  }
}
