import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyMockFormComponent } from './body-mock-form.component';

describe('BodyMockFormComponent', () => {
  let component: BodyMockFormComponent;
  let fixture: ComponentFixture<BodyMockFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyMockFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyMockFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
