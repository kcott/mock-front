import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlMockFormComponent } from './url-mock-form.component';

describe('UrlMockFormComponent', () => {
  let component: UrlMockFormComponent;
  let fixture: ComponentFixture<UrlMockFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlMockFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlMockFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
