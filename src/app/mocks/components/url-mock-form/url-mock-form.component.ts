import { Component, OnInit, Input, Output } from '@angular/core';
import { MocksService } from '../../services/mocks.service';
import { UrlMock } from '../../dtos/UrlMock';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';

@Component({
  selector: 'app-url-mock-form',
  templateUrl: './url-mock-form.component.html',
  styleUrls: ['./url-mock-form.component.scss']
})
export class UrlMockFormComponent implements OnInit {
  protected id: number;
  protected name: string;
  protected url: string;
  protected description: string;
  protected active: boolean;

  @Input() protected bsModalRef: BsModalRef;
  @Input() protected urlMock: UrlMock;
  @Input() protected idUrlMock: number;

  constructor(
    private mocksService: MocksService
  ) { }

  ngOnInit() {
    this.updateUrlMock(this.urlMock);
  }

  updateUrlMock(urlMock: UrlMock) {
    if (urlMock) {
      this.id = urlMock.id;
      this.name = urlMock.name;
      this.url = urlMock.url;
      this.description = urlMock.description;
      this.active = urlMock.active;
    }
  }

  onSubmit() {
    const myUrlMock: UrlMock = {
      id: this.id,
      name: this.name,
      url: this.url,
      description: this.description,
      active: this.active,
      bodyMocks: undefined
    };
    if (!this.urlMock) {
      this.mocksService.addUrlMock(myUrlMock).subscribe(urlMock =>
        this.success(urlMock)
      );
    } else {
      this.mocksService.updateUrlMock(myUrlMock).subscribe(urlMock =>
        this.success(urlMock)
      );
    }
  }

  success(urlMock: UrlMock) {
    let urlMockOnMemory = this.mocksService.urlMocks.find(url => urlMock.id === url.id);
    if (urlMockOnMemory !== undefined) {
      urlMockOnMemory.name = urlMock.name;
      urlMockOnMemory.description = urlMock.description;
      urlMockOnMemory.url = urlMock.url;
      urlMockOnMemory.active = urlMock.active;
    } else {
      this.mocksService.urlMocks.push(urlMock);
    }
    this.bsModalRef.hide();
  }
}
