import { Component, OnInit, Input, TemplateRef, DoCheck } from '@angular/core';
import { MocksService } from '../../services/mocks.service';
import { ActivatedRoute } from '@angular/router';
import { BodyMock } from '../../dtos/BodyMock';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UrlMock } from '../../dtos/UrlMock';
import { RequestMethodService } from 'src/app/generics/services/request-method.service';
import { RequestMethodMockEnum } from 'src/app/generics/enums/RequestMethodMockEnum';

@Component({
  selector: 'app-body-mock',
  templateUrl: './body-mock.component.html',
  styleUrls: ['./body-mock.component.scss']
})
export class BodyMockComponent implements OnInit, DoCheck {
  protected bsModalRef: BsModalRef;

  protected urlMock: UrlMock;
  protected bodyMock: BodyMock;
  protected idUrl;
  protected idBody;

  constructor(
    private bsModalService: BsModalService,
    private mocksService: MocksService,
    private requestMethodService: RequestMethodService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.idUrl = this.route.snapshot.paramMap.get('idUrl');
    this.idBody = +this.route.snapshot.paramMap.get('idBody');
  }

  ngDoCheck(): void {
    if (this.mocksService.urlMocks !== undefined && this.bodyMock === undefined) {
      this.urlMock = this.mocksService.urlMocks.find(urlMock => urlMock.id === this.idUrl);
      if (this.urlMock) {
        this.bodyMock = this.urlMock.bodyMocks.find(bodyMock => bodyMock.id === this.idBody);
      }
    }
  }

  protected addBodyMock() {
    this.mocksService.addBodyMock(this.bodyMock).subscribe(bodyMock =>
      this.bodyMock = bodyMock
    );
  }

  protected edit(template: TemplateRef<any>) {
    this.bsModalRef = this.bsModalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' })
    );
  }

  protected requestMethodContentCssClass(bodyMock: BodyMock) {
    let returnClasses: string;
    const cssClasses = this.requestMethodService.getCssClasses(RequestMethodMockEnum[bodyMock.requestMethod]);
    returnClasses = cssClasses[this.requestMethodService.cssClassKeys.CONTENT];
    if (!bodyMock.active) {
      returnClasses = returnClasses + ' stripes';
    }
    return returnClasses;
  }

  protected requestMethodVerbCssClass(bodyMock: BodyMock) {
    const cssClasses = this.requestMethodService.getCssClasses(RequestMethodMockEnum[bodyMock.requestMethod]);
    return cssClasses[this.requestMethodService.cssClassKeys.VERB];
  }

  protected cssClassUrlMock(urlMock: UrlMock) {
    if (!urlMock.active) {
      return 'stripes';
    }
  }
}
