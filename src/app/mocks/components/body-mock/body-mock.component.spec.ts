import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyMockComponent } from './body-mock.component';

describe('BodyMockComponent', () => {
  let component: BodyMockComponent;
  let fixture: ComponentFixture<BodyMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyMockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
