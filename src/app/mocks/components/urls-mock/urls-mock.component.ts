import { Component, OnInit, TemplateRef, DoCheck } from '@angular/core';
import { MocksService } from 'src/app/mocks/services/mocks.service';
import { UrlMock } from '../../dtos/UrlMock';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-urls-mock',
  templateUrl: './urls-mock.component.html',
  styleUrls: ['./urls-mock.component.scss']
})
export class UrlsMockComponent implements OnInit, DoCheck {
  private bsModalRef: BsModalRef;

  protected urlMocks: UrlMock[];

  constructor(
    private bsModalService: BsModalService,
    private mocksService: MocksService
  ) { }

  ngOnInit() {
    this.urlMocks = this.mocksService.urlMocks;
  }

  ngDoCheck(): void {
    if (this.mocksService.urlMocks !== undefined && this.urlMocks === undefined) {
      this.urlMocks = this.mocksService.urlMocks;
    }
  }

  protected edit(template: TemplateRef<any>) {
    this.bsModalRef = this.bsModalService.show(template);
  }

  protected delete(id: number) {
    this.mocksService.deleteUrlMock(id).subscribe();
    this.mocksService.urlMocks.splice(
      this.mocksService.urlMocks.indexOf(
        this.mocksService.urlMocks.find(url => url.id == id)
    ), 1);
  }

  protected cssClassUrlMock(urlMock: UrlMock) {
    if (!urlMock.active) {
      return 'stripes';
    }
  }
}
