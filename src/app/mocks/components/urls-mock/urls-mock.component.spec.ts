import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlsMockComponent } from './urls-mock.component';

describe('UrlsMockComponent', () => {
  let component: UrlsMockComponent;
  let fixture: ComponentFixture<UrlsMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlsMockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlsMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
