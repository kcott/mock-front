import { NgModule } from '@angular/core';
import { NavigationEnd, Router, RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MockComponent } from './components/mock/mock.component';

const mocksRoutes: Routes = [
  {
    path: 'mocks/urls',
    component: MockComponent
  },
  {
    path: 'mocks/url/:idUrl',
    component: MockComponent
  },
  {
    path: 'mocks/url/:idUrl/body/new',
    component: MockComponent
  },
  {
    path: 'mocks/url/:idUrl/body/:idBody',
    component: MockComponent
  },
  {
    path: '',
    redirectTo: 'mocks/urls',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(mocksRoutes, { onSameUrlNavigation: 'reload' })
  ],
  exports: [
    RouterModule
  ]
})
export class MocksRoutingModule {

  constructor(private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }
}
