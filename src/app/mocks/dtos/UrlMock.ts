import { BodyMock } from './BodyMock';

export class UrlMock {
    id: number;
    name: string;
    url: string;
    description: string;
    active: boolean;
    bodyMocks: BodyMock[];
}
