export class BodyMock {
    id: number;
    urlMockId: number;
    name: string;
    description: string;
    requestMethod: string;
    active: boolean;
    requestBody: string;
    responseBody: string;
}
