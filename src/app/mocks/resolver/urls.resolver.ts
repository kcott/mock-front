import { Injectable } from '@angular/core';
import { MocksService } from '../services/mocks.service';
import { Resolve } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UrlsResolver implements Resolve<void> {

  constructor(private mocksService: MocksService) { }

  resolve(route: import('@angular/router').ActivatedRouteSnapshot, state: import('@angular/router').RouterStateSnapshot) {
    //this.mocksService.loadUrlMocks();
  }
}
