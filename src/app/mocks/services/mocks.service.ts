import { Injectable } from '@angular/core';
import { UrlMock } from '../dtos/UrlMock';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { BodyMock } from '../dtos/BodyMock';
import { url } from 'inspector';

@Injectable()
export class MocksService {

    public urlMocks: UrlMock[];

    constructor(private http: HttpClient) {
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            console.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        };
    }

    public getServerUrl() {
        return 'http://localhost:8080/';
    }

    public getMockUrl() {
        return `${this.getServerUrl()}mocks/mock/`;
    }

    public getUrlMocks(): Observable<UrlMock[]> {
        return this.http.get<UrlMock[]>(`${this.getServerUrl()}mocks/urls`)
            .pipe(
                catchError(this.handleError<UrlMock[]>('MocksService.urlMocks', []))
            );
    }

    public addUrlMock(urlMock: UrlMock): Observable<UrlMock> {
        return this.http.put<UrlMock>(`${this.getServerUrl()}mocks/url/add`, JSON.parse(JSON.stringify(urlMock)))
            .pipe(
                catchError(this.handleError<UrlMock>('MocksService.addUrlMock', undefined))
            );
    }

    public updateUrlMock(urlMock: UrlMock): Observable<UrlMock> {
        return this.http.post<UrlMock>(`${this.getServerUrl()}mocks/url/update`, JSON.parse(JSON.stringify(urlMock)))
            .pipe(
                catchError(this.handleError<UrlMock>('MocksService.updateUrlMock', undefined))
            );
    }

    public deleteUrlMock(id: number): Observable<any> {
        return this.http.delete(`${this.getServerUrl()}mocks/url/delete/` + id)
            .pipe(
                catchError(this.handleError<any>('MocksService.deleteUrlMock', undefined))
            );
    }

    public addBodyMock(bodyMock: BodyMock): Observable<BodyMock> {
        return this.http.put<BodyMock>(`${this.getServerUrl()}mocks/url/body/add`, JSON.parse(JSON.stringify(bodyMock)))
            .pipe(
                catchError(this.handleError<BodyMock>('MocksService.addBodyMock', undefined))
            );
    }

    public updateBodyMock(bodyMock: BodyMock): Observable<BodyMock> {
        return this.http.put<BodyMock>(`${this.getServerUrl()}mocks/url/body/update`, JSON.parse(JSON.stringify(bodyMock)))
            .pipe(
                catchError(this.handleError<BodyMock>('MocksService.updateBodyMock', undefined))
            );
    }

    public deleteBodyMock(id: number): Observable<any> {
        return this.http.delete(`${this.getServerUrl()}mocks/url/body/delete/` + id)
            .pipe(
                catchError(this.handleError<any>('MocksService.deleteBodyMock', undefined))
            );
    }
}
